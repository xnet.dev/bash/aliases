#!/usr/bin/env bash

#
###
######    x.BASH    2017 - 2023    x.NET Development
###
#

. ~/aliases/bin/aliases.sh;
. ~/aliases/bin/design.sh;
. ~/aliases/bin/apps-index.sh;
xtsh-ascii

# Change prompt for root user
prompt_symbol="${EUID:+\033[38m at ${info_color}}"

# PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ \n       '
PS1='${debian_chroot:+($debian_chroot)}\n\['${info_color}'\] \u \['${prompt_symbol}'\] \h \['${prompt_color}'\] [\['${reset}'\]\w\['${prompt_color}'\]]\n\['${reset}'\]'