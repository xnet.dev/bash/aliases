# xBASH
As most of the Linux-users work with the terminal, some have a personal file to get the own workflow right.
This file is called .bash_aliases and it's loaded right after the .bashrc file every time you open a new terminal-session.
As this structure allows us to overwrite many statements and variables from the standalone options of a BASH Shell, you can modify the habits of the BASH itself.

To get started with the xnet.dev-aliases setup, link `aliases/.bash_aliases` to your homefolder as `.bash_aliases` and use the command `source .bashrc`

Use this command to install at your BASH-Home

`cd ~ && git clone http://gitlab.com/xnet.dev/bash/aliases && ln -s aliases/.bash_aliases . && source .bashrc`


# Navigation Features
| Alias / Function | Usage / Definition |
| ---  | --- |
| l | Alias for: grc ls -lh |
| ll | Alias for: grc ls -lah |
| lg SEARCH | Alias for: grep SEARCJ in ls -lah|


# Teamwork
| Alias / Function | Usage / Definition |
| ---  | --- |
| x.cl | clear screen and call ascii-header |


# Administration
| Alias / Function | Usage / Definition |
| ---  | --- |
| osupgrade | Alias for: sudo apt update && sudo apt upgrade -y |
| x.logoff | clean logout, removes bash-history |
| x.ip | shows all local ips of the system |



# Changelog

05AUG2023 - v0.1b
- added grc to some aliases
- removed characters from PS1 variable

04MAR2023 - v0.1a
- added folders for modules
- added file-handling

14JUL2022 - v0.0d
- adjustments to the aliases
- updated Readme because it was outdated

20DEC2020 - v0.0c
- added prefix and updated README according to changes

23JUL2018 - v0.0b - TidyUp
- removed osinstall since it doesn't help much
- updated README.md / xbash in sturcture

02MAY2018 - v0.0a - First Push
- file "main" contains the actual .bash_aliases-content
